/**********************************************
* Description: Main class which starts when page loads
*
*
* Author: branco
**********************************************/
///<reference path='AllReferences.ts' />
var Homepage;
(function (Homepage) {
    var serverFlow = ServerFlow;
    var Main = (function () {
        /**
        * Constructor of our class
        */
        function Main() {
            /** Default selection on Category drop down*/
            this._categoryDefaultValue = "Izaberite oblast";
            /** Default selection on Options drop down*/
            this._optionsDefaultValue = "Izaberite vrstu posla";
            var self = this;
            self.initialize();
            self.loadFromServer();
            self.createEventListener();
            self.createDropDownOptions();
            self.testResponseList = [];
            console.log("Main Init");
        }
        /**
        * Initialize our elements from the dome
        */
        Main.prototype.initialize = function () {
            var self = this;
            self._categoryHtmlElement = document.getElementById("select1");
            self._optionsHtmlElement = document.getElementById("select2");
            self._nextButton = document.getElementById("nextButton");
            self._warningStringCategory = document.getElementById("warningStringCategory");
            self._warningStringOptions = document.getElementById("warningStringOptions");
            self._signUpButton = document.getElementById("signContractor");
            self._categoriesRecieved = [];
            self._subCategoriesRecieved = [];
        };
        /**
        * Fatch from server our dropdown categories.
        * on success populate and create our DropDownCateogry list
        */
        Main.prototype.createDropDownCategory = function (categories) {
            // currently the values are Mocked;
            var self = this;
            var mockedValues = ["Cat1", "Cat2", "Cat3", "Cat4", "Cat5"];
            self._dropDownCategory = new Homepage.DropDownMenuVE(self._categoryHtmlElement, ((categories == undefined) ? mockedValues : categories));
        };
        /**
        * fetch from server our dropdown options.
        * on success populate and create our DropDownCateogry list
        */
        Main.prototype.createDropDownOptions = function (categories) {
            // currently the values are Mocked;
            var self = this;
            var mockedValues = ["Opt1", "Opt2", "Opt3", "Opt4", "Opt5"];
            self._dropDownOptions = new Homepage.DropDownMenuVE(self._optionsHtmlElement, (categories == undefined ? mockedValues : categories));
        };
        /**
        * On Next button click go to next step if all data is collected
        */
        Main.prototype.nextButtonClicked = function () {
            var self = this;
            var request;
            {
                text: "test request";
            }
            ;
            $.ajax({
                type: "GET",
                crossDomain: true,
                crossOrigin: true,
                url: 'http://localhost:2971/api/Values',
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },
                success: function (dataMain, status) {
                    self.onSuccess(dataMain, self);
                },
                error: function (request, status, error) {
                    // TODO: Remove 
                    alert("error1: " + request.responseText);
                    // We should log if error response is recieved.
                    // For now we do nothing 
                }
            });
            return true;
        };
        /**
         * On successfull response we should store the data recieved in our list
         */
        Main.prototype.onSuccess = function (dataMain, context) {
            if (dataMain) {
                for (var i = 0; i < dataMain.length; i++) {
                    context.testResponseList.push(new serverFlow.Response1(dataMain[i].id, dataMain[i].name));
                }
            }
        };
        /**
        * Check if user has selected category and options drop down
        */
        Main.prototype.verifySelections = function () {
            var self = this;
            return ((self._dropDownCategory.getSelection() != self._categoryDefaultValue) && (self._dropDownOptions.getSelection() != self._optionsDefaultValue));
        };
        /**
        * Show warning message
        */
        Main.prototype.showWarningMessage = function () {
            var self = this;
            // Warning for categories
            if (self._dropDownCategory.getSelection() == self._categoryDefaultValue) {
                self._warningStringCategory.style.visibility = 'visible';
            }
            else {
                self._warningStringCategory.style.visibility = 'hidden';
            }
            // Warning for options 
            if (self._dropDownOptions.getSelection() == self._optionsDefaultValue) {
                self._warningStringOptions.style.visibility = 'visible';
            }
            else {
                self._warningStringOptions.style.visibility = 'hidden';
            }
        };
        Main.prototype.createRequestForm = function () {
        };
        /**
        * Create event listeners for our buttons
        */
        Main.prototype.createEventListener = function () {
            var self = this;
            console.log("Event listeners");
            self._nextButton.addEventListener('click', function () {
                return self.nextButtonClicked();
            });
            self._signUpButton.addEventListener('click', function () {
                return self.signUpButtonClicked();
            });
            // change on dropdown menu
            self._categoryHtmlElement.addEventListener('change', function () {
                return self.categoryChange();
            });
            // change on sub menu dropdown
            self._optionsHtmlElement.addEventListener('change', function () {
                return;
            });
        };
        /**
        * Category drow down has changed
        */
        Main.prototype.categoryChange = function () {
            var self = this;
            // cateogry list to be put into dropdown menu
            var subCategoriesList = [];
            // Go through all subcategories and check if our parent is currently selected category
            for (var i = 0; i < self._subCategoriesRecieved.length; i++) {
                if (self._dropDownCategory.getSelection() == self._subCategoriesRecieved[i].parentCategory) {
                    subCategoriesList.push(self._subCategoriesRecieved[i].category);
                }
            }
            self._dropDownOptions.updateList(subCategoriesList);
        };
        /**
        * Sign up button is pressed
        */
        Main.prototype.signUpButtonClicked = function () {
            window.location.href = 'post-job-step2.html';
        };
        /**
        * Get the data from the server
        */
        Main.prototype.loadFromServer = function () {
            /*var self = this;
            $.ajax({

                url: 'http://localhost:8080/jobsMain.php',
                success: function (dataMain) {
                    $('.result').html(dataMain);
                    self.initialiseCategories(dataMain);
                    self.loadSubCategories();
                },
                error: function (request, status, error) {
                    alert("error1: " + request.responseText);
                }
            });*/
        };
        /**
        * Get subcategories from the server
        */
        Main.prototype.loadSubCategories = function () {
            var self = this;
            $.ajax({
                url: 'http://localhost:8080/jobsSubCat.php',
                success: function (dataSubCat) {
                    $('.result').html(dataSubCat);
                    //console.log("Successfully loaded sub categories");
                    self.initialiseSubCategories(dataSubCat);
                },
                error: function (request, status, error) {
                    alert("error2: " + request.responseText);
                    //console.log("LoadFromServer error");
                }
            });
        };
        /**
        * Initialise our categories
        */
        // TODO: Create iCategories interface for catogroies
        Main.prototype.initialiseCategories = function (dataMain) {
            var self = this;
            var mainCategories = dataMain;
            var categoriesList = [];
            var i = 0;
            while (mainCategories[i]) {
                // All our categories in one string array
                categoriesList.push(mainCategories[i].Category);
                var categoryObject = {
                    category: mainCategories[i].Category,
                    parentCategory: mainCategories[i].Parent,
                };
                self._categoriesRecieved.push(categoryObject);
                i++;
            }
            // initialise our dropdown
            self.createDropDownCategory(categoriesList);
        };
        /**
        * Initialise our categories which are dependant on our parent category
        */
        Main.prototype.initialiseSubCategories = function (dataMain) {
            var self = this;
            var mainSubCategories = dataMain;
            var subCategoriesList = [];
            var i = 0;
            while (mainSubCategories[i]) {
                var subCategoryObject = {
                    category: mainSubCategories[i].Category,
                    parentCategory: mainSubCategories[i].Parent,
                };
                self._subCategoriesRecieved.push(subCategoryObject);
                i++;
            }
        };
        return Main;
    })();
    Homepage.Main = Main;
    /**
    * Register event listener for window size
    */
    document.addEventListener("DOMContentLoaded", function () {
        //var main = new Main();
        console.log("DomContentLoaded");
    });
    function start() {
        var main = new Main();
        console.log("Initializing main");
    }
    window.onload = start;
    /**
    * Capture on window resize event
    * Adjust the css classes accordingly
    */
    //$(new Function("var game = new Main();"));
    function onWindowResize() {
        var w = window.outerWidth;
        var h = window.outerHeight;
        var txt = "Window size: width=" + w + ", height=" + h;
        //console.log(txt);
    }
})(Homepage || (Homepage = {}));
//# sourceMappingURL=app.js.map