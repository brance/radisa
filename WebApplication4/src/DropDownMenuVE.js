/************************************************************************************
* Description: Drop down lists class, which should store every information about drop down selectors
* File: DropDownMenuVE.ts
* Author: branco
************************************************************************************/
///<reference path='AllReferences.ts' />
var Homepage;
(function (Homepage) {
    var DropDownMenuVE = (function () {
        function DropDownMenuVE(ourCategoryHtmlElement, initialList) {
            this._const = "Izaberite vrstu posla";
            var self = this;
            self._myHtmlElement = ourCategoryHtmlElement;
            // Basicly this part is used for Mocked values for testing
            if (initialList) {
                self._list = initialList;
            }
            self.populateList();
            self.addEventListeners();
        }
        /**
        * Populate our list with values
        */
        DropDownMenuVE.prototype.populateList = function () {
            var self = this;
            var selection;
            for (var i = 0; i < self._list.length; i++) {
                selection = self._list[i];
                var el = document.createElement("option");
                el.textContent = selection;
                el.value = selection;
                self._myHtmlElement.appendChild(el);
            }
        };
        /**
        * Clear our selector
        */
        DropDownMenuVE.prototype.clearList = function () {
            var self = this;
            var selector = document.getElementById(self._id);
            var length = self._myHtmlElement.options.length;
            while (self._myHtmlElement.options.length > 1) {
                self._myHtmlElement.remove(1);
            }
        };
        /**
        * Return currently selected value;
        */
        DropDownMenuVE.prototype.getSelection = function () {
            var self = this;
            return self._myHtmlElement.options[self._myHtmlElement.selectedIndex].text;
        };
        /**
        * Add event listeners to our drop down menu
        */
        DropDownMenuVE.prototype.addEventListeners = function () {
            var self = this;
            self._myHtmlElement.addEventListener("change", function () {
                // we can catch the event here
            });
        };
        /**
        * Update our current list
        */
        DropDownMenuVE.prototype.updateList = function (newList) {
            var self = this;
            self.clearList();
            self._list = newList;
            self.populateList();
        };
        return DropDownMenuVE;
    })();
    Homepage.DropDownMenuVE = DropDownMenuVE;
})(Homepage || (Homepage = {}));
//# sourceMappingURL=DropDownMenuVE.js.map