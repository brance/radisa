﻿/**********************************************
* Description: Response form recieved from the server 
* this is just for test purposes
*
* Author: branco
**********************************************/

///<reference path='../AllReferences.ts' />

module ServerFlow
{
	export class Response1
	{
		/** our id */
		public id: number;

		/** our name */
		public name: string;

		/**
		* Constructor of our class
		*/
		constructor(arg_id: number, arg_name: string)
		{
			this.id = arg_id;
			this.name = arg_name;
		}
	}
}