/************************************************************************************
* Description: This class is used for filling out the job informations and sending it to server
* File: RequestFormVE.ts
* Author: branco
************************************************************************************/
///<reference path='AllReferences.ts' />
var Request;
(function (Request) {
    var InputFields = (function () {
        function InputFields() {
        }
        return InputFields;
    }());
    Request.InputFields = InputFields;
    var RequestFormVE = (function () {
        function RequestFormVE() {
            /** Constant used as url parameter for categories*/
            this.categoryTag = "Cat";
            /** Constant used as url parameter for categories*/
            this.optionsTag = "SubCat";
            var self = this;
            self._inputFields = [];
            self.initialize();
            self.addEventListeners();
            self.getValuesFromURL();
        }
        /**
        * Initialise with all elements from the dome
        */
        RequestFormVE.prototype.initialize = function () {
            var self = this;
            var inputField = new InputFields;
            inputField.fieldName = "city";
            inputField.inputBox = document.getElementById("cityTextBox");
            inputField.warningParagraph = document.getElementById("warningStringCity");
            self._inputFields.push(inputField);
            inputField = new InputFields;
            inputField.fieldName = "jobDescription";
            inputField.inputBox = document.getElementById("jobDescriptionTextBox");
            inputField.warningParagraph = document.getElementById("warningStringJobDescription");
            self._inputFields.push(inputField);
            inputField = new InputFields;
            inputField.fieldName = "name";
            inputField.inputBox = document.getElementById("nameTextBox");
            inputField.warningParagraph = document.getElementById("warningStringName");
            self._inputFields.push(inputField);
            inputField = new InputFields;
            inputField.fieldName = "surname";
            inputField.inputBox = document.getElementById("surnameTextBox");
            inputField.warningParagraph = document.getElementById("warningStringSurname");
            self._inputFields.push(inputField);
            inputField = new InputFields;
            inputField.fieldName = "phone";
            inputField.inputBox = document.getElementById("phoneNumberTextBox");
            inputField.warningParagraph = document.getElementById("warningStringPhoneNumber");
            inputField.warningParagraphFormat = document.getElementById("warningStringPhoneNumberFormat");
            self._inputFields.push(inputField);
            inputField = new InputFields;
            inputField.fieldName = "email";
            inputField.inputBox = document.getElementById("emailAddressTextBox");
            inputField.warningParagraph = document.getElementById("warningStringEmailAddress");
            inputField.warningParagraphFormat = document.getElementById("warningStringEmailAddressFormat");
            self._inputFields.push(inputField);
            self.sendRequestButton = document.getElementById("sendRequestButton");
            self.backButton = document.getElementById("backButton");
        };
        /**
        * Set event listeners to buttons;
        */
        RequestFormVE.prototype.addEventListeners = function () {
            var self = this;
            self.sendRequestButton.addEventListener('click', function () {
                return self.insertNewJob();
            });
            self.backButton.addEventListener('click', function () {
                return self.goBack();
            });
        };
        /**
        * Back button pressed
        */
        RequestFormVE.prototype.goBack = function () {
            var self = this;
            window.history.back();
        };
        /**
        * Read sent values from URL
        */
        RequestFormVE.prototype.getValuesFromURL = function () {
            var self = this;
            self.category = self.getUrlParameter(self.categoryTag);
            self.options = self.getUrlParameter(self.optionsTag);
            console.log(self.category);
            console.log(self.options);
        };
        /**
        * Get parameters
        */
        RequestFormVE.prototype.getUrlParameter = function (sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)), sURLVariables = sPageURL.split('&'), sParameterName, i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        /**
        * Check if user has entered all obligatory fields
        */
        RequestFormVE.prototype.verifySelections = function () {
            var self = this;
            var error = false;
            for (var i = 0; i < self._inputFields.length; i++) {
                if (self._inputFields[i].inputBox.value == "") {
                    // Show warning
                    error = true;
                    self._inputFields[i].warningParagraph.style.visibility = 'visible';
                }
                else {
                    self._inputFields[i].warningParagraph.style.visibility = 'hidden';
                }
            }
            if (error) {
                return false;
            }
            // Final check: formating of number and e-mail
            for (var i = 0; i < self._inputFields.length; i++) {
                if (self._inputFields[i].warningParagraphFormat != undefined) {
                }
            }
            return true;
        };
        /**
        * Write the data to our server
        */
        RequestFormVE.prototype.insertNewJob = function () {
            var self = this;
            if (self.verifySelections()) {
                $.ajax({
                    type: "POST",
                    url: 'http://localhost:8080/insertJob.php',
                    contentType: "application/json; charset=utf-8",
                    data: {
                        city: self._inputFields[0].inputBox.value,
                        comment: self._inputFields[1].inputBox.value,
                        name: self._inputFields[2].inputBox.value,
                        surname: self._inputFields[3].inputBox.value,
                        phone: self._inputFields[4].inputBox.value,
                        email: self._inputFields[5].inputBox.value,
                    },
                    dataType: "json",
                });
            }
        };
        return RequestFormVE;
    }());
    Request.RequestFormVE = RequestFormVE;
})(Request || (Request = {}));
//# sourceMappingURL=RequestFormVE.js.map