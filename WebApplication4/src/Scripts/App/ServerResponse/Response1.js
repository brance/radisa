/**********************************************
* Description: Response form recieved from the server
* this is just for test purposes
*
* Author: branco
**********************************************/
///<reference path='../AllReferences.ts' />
var ServerFlow;
(function (ServerFlow) {
    var Response1 = (function () {
        /**
        * Constructor of our class
        */
        function Response1(arg_id, arg_name) {
            this.id = arg_id;
            this.name = arg_name;
        }
        return Response1;
    }());
    ServerFlow.Response1 = Response1;
})(ServerFlow || (ServerFlow = {}));
//# sourceMappingURL=Response1.js.map