﻿/************************************************************************************
* Description: Drop down lists class, which should store every information about drop down selectors
* File: DropDownMenuVE.ts
* Author: branco
************************************************************************************/

///<reference path='AllReferences.ts' />

module Homepage {
	export class DropDownMenuVE {
		private _const: string = "Izaberite vrstu posla";
		/** Strings to be displayed in list*/
		private _list: string[];

		/** Id in the dome: so we could populate this element*/
		private _id: string;

		/** Our element in dome*/
		private _myHtmlElement: HTMLSelectElement;

		/** Our selected element*/
		public selectedElement: number;

		constructor(ourCategoryHtmlElement: HTMLSelectElement, initialList?: string[]) {
			var self = this;

			self._myHtmlElement = ourCategoryHtmlElement;

			// Basicly this part is used for Mocked values for testing
			if (initialList) {
				self._list = initialList;
			}

			self.populateList();
			self.addEventListeners();
		} 

		/**
		* Populate our list with values
		*/
		private populateList(): void {
			var self = this;

			var selection: string;
			for (var i = 0; i < self._list.length; i++) {
				selection = self._list[i];
				var el = document.createElement("option");
				el.textContent = selection;
				el.value = selection;
				self._myHtmlElement.appendChild(el);
			}
		}

		/**
		* Clear our selector
		*/
		private clearList(): void {
			var self = this;

			var selector = document.getElementById(self._id);
			var length = self._myHtmlElement.options.length;

			while (self._myHtmlElement.options.length > 1) {
				self._myHtmlElement.remove(1);
			}
		}

		/**
		* Return currently selected value;
		*/
		public getSelection(): string {
			var self = this;
			return self._myHtmlElement.options[self._myHtmlElement.selectedIndex].text;
		}

		/**
		* Add event listeners to our drop down menu
		*/
		public addEventListeners() {
			var self = this;
			self._myHtmlElement.addEventListener("change", function () {
				// we can catch the event here
			});
		}

		/**
		* Update our current list
		*/
		public updateList(newList?: string[]): void {
			var self = this;
			self.clearList();

			self._list = newList;

			self.populateList()
		}
	}
}