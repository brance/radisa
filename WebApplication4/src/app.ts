﻿/**********************************************
* Description: Main class which starts when page loads
*
*
* Author: branco
**********************************************/

///<reference path='AllReferences.ts' />

module Homepage 
{
    import serverFlow = ServerFlow;

	/** Define how our data for Categories looks like*/
	export interface ICategoriesData {
		category: string;
		parentCategory: string;
	}

	interface iRequest
	{
		text: string;
	}


	export class Main {
		/** Default selection on Category drop down*/
		private _categoryDefaultValue: string = "Izaberite oblast";

		/** Default selection on Options drop down*/
		private _optionsDefaultValue: string = "Izaberite vrstu posla";

		/** Our categories drop down list */
		private _dropDownCategory: DropDownMenuVE;

		/** Our options drop down list */
		private _dropDownOptions: DropDownMenuVE;

		/** Our categories HTML element */
		private _categoryHtmlElement: HTMLSelectElement;

		/** Our options HTML element */
		private _optionsHtmlElement: HTMLSelectElement;

		/** Next button */
		private _nextButton: HTMLElement;

		/** Sign up button*/
		private _signUpButton: HTMLElement;

		/** Warning paragraph for category drop down */
		private _warningStringCategory: HTMLElement;

		/** Warning paragraph for options drop down */
		private _warningStringOptions: HTMLElement;

		/** List of all categories */
		private _categoriesRecieved: ICategoriesData[];

		/** List of all subcategories */
		private _subCategoriesRecieved: ICategoriesData[];

		/** List recieved from the server*/
		private testResponseList: ServerFlow.Response1[];

		/**
		* Constructor of our class
		*/
		constructor() {
			var self = this;

			self.initialize();
			self.loadFromServer();
			self.createEventListener();
			self.createDropDownOptions();
			self.testResponseList = [];
			console.log("Main Init");
		}

		/**
		* Initialize our elements from the dome
		*/
		private initialize(): void {
			var self = this;
			self._categoryHtmlElement = <HTMLSelectElement> document.getElementById("select1");
			self._optionsHtmlElement = <HTMLSelectElement> document.getElementById("select2");
			self._nextButton = <HTMLElement> document.getElementById("nextButton");
			self._warningStringCategory = <HTMLElement> document.getElementById("warningStringCategory");
			self._warningStringOptions = <HTMLElement> document.getElementById("warningStringOptions");
			self._signUpButton = <HTMLElement> document.getElementById("signContractor");
			self._categoriesRecieved = []
			self._subCategoriesRecieved = [];
		}

		/**
		* Fatch from server our dropdown categories. 
		* on success populate and create our DropDownCateogry list
		*/
		private createDropDownCategory(categories?: string[]): void {
			// currently the values are Mocked;
			var self = this;
			var mockedValues: string[] = ["Cat1", "Cat2", "Cat3", "Cat4", "Cat5"];
			self._dropDownCategory = new DropDownMenuVE(self._categoryHtmlElement, ((categories == undefined) ? mockedValues : categories));
		}

		/**
		* fetch from server our dropdown options. 
		* on success populate and create our DropDownCateogry list
		*/
		private createDropDownOptions(categories?: string[]): void {
			// currently the values are Mocked;
			var self = this;
			var mockedValues: string[] = ["Opt1", "Opt2", "Opt3", "Opt4", "Opt5"];
			self._dropDownOptions = new DropDownMenuVE(self._optionsHtmlElement, (categories == undefined ? mockedValues : categories));
		}

		/**
		* On Next button click go to next step if all data is collected
		*/
		public nextButtonClicked(): boolean {
			var self = this;
           
			$.ajax({
				type: "GET",
				url: 'http://localhost:2971/api/Values',
                success: function (dataMain: ServerFlow.Response1[], status: any)
				{
					self.onSuccess(dataMain, self);
				},
				
				error: function (request: any, status: any, error: any) 
				{
					// TODO: Remove 
					alert("error1: " + request.responseText);
					// We should log if error response is recieved.
					// For now we do nothing 
				}
			});
            return true;

            /// this is test edit for commit 
		}

		/** 
		 * On successfull response we should store the data recieved in our list 
		 */
		public onSuccess(dataMain: ServerFlow.Response1[], context: Main)
		{
			if (dataMain)
			{
				for (var i: number = 0; i < dataMain.length; i++)
				{
					context.testResponseList.push(new serverFlow.Response1(dataMain[i].id, dataMain[i].name));
				}
			}
		}

		/**
		* Check if user has selected category and options drop down
		*/
		private verifySelections(): boolean {
			var self = this;

			return ((self._dropDownCategory.getSelection() != self._categoryDefaultValue) && (self._dropDownOptions.getSelection() != self._optionsDefaultValue));
		}

		/** 
		* Show warning message
		*/
		private showWarningMessage(): void {
			var self = this;

			// Warning for categories
			if (self._dropDownCategory.getSelection() == self._categoryDefaultValue) {
				self._warningStringCategory.style.visibility = 'visible';
			}
			else {
				self._warningStringCategory.style.visibility = 'hidden';
			}

			// Warning for options 
			if (self._dropDownOptions.getSelection() == self._optionsDefaultValue) {
				self._warningStringOptions.style.visibility = 'visible';
			}
			else {
				self._warningStringOptions.style.visibility = 'hidden';
			}
		}

		private createRequestForm(): void {

		}

		/**
		* Create event listeners for our buttons
		*/
		private createEventListener(): void {
			var self = this;
			console.log("Event listeners");

			self._nextButton.addEventListener('click', function () {
				return self.nextButtonClicked();
			});

			self._signUpButton.addEventListener('click', function () {
				return self.signUpButtonClicked();
			});

			// change on dropdown menu
			self._categoryHtmlElement.addEventListener('change', function () {
				return self.categoryChange();
			});

			// change on sub menu dropdown
			self._optionsHtmlElement.addEventListener('change', function () {
				return;
			});
		}

		/**
		* Category drow down has changed
		*/
		private categoryChange(): void {
			var self = this;

			// cateogry list to be put into dropdown menu
			var subCategoriesList: string[] = [];

			// Go through all subcategories and check if our parent is currently selected category
			for (var i = 0; i < self._subCategoriesRecieved.length; i++) {
				if (self._dropDownCategory.getSelection() == self._subCategoriesRecieved[i].parentCategory) {
					subCategoriesList.push(self._subCategoriesRecieved[i].category);
				}
			}
			self._dropDownOptions.updateList(subCategoriesList);
		}

		/**
		* Sign up button is pressed 
		*/
		private signUpButtonClicked(): void {
			window.location.href = 'post-job-step2.html';
		}

		/**
		* Get the data from the server
		*/
		private loadFromServer(): void {
			/*var self = this;
			$.ajax({

				url: 'http://localhost:8080/jobsMain.php',
				success: function (dataMain) {
					$('.result').html(dataMain);
					self.initialiseCategories(dataMain);
					self.loadSubCategories();
				},
				error: function (request, status, error) {
					alert("error1: " + request.responseText);
				}
			});*/

		}
		/**
		* Get subcategories from the server
		*/
		private loadSubCategories(): void {
			var self = this;
			$.ajax({
				url: 'http://localhost:8080/jobsSubCat.php',
				success: function (dataSubCat: any) {
					$('.result').html(dataSubCat);
					//console.log("Successfully loaded sub categories");
					self.initialiseSubCategories(dataSubCat);
				},
				error: function (request: any, status: any, error: any) {
					alert("error2: " + request.responseText);
					//console.log("LoadFromServer error");
				}
			});
		}

		/**
		* Initialise our categories 
		*/
		// TODO: Create iCategories interface for catogroies
		private initialiseCategories(dataMain: any): void {
			var self = this;

			var mainCategories: any = dataMain;

			var categoriesList: string[] = [];

			var i: number = 0;
			while (mainCategories[i]) {
				// All our categories in one string array
				categoriesList.push(mainCategories[i].Category);

				var categoryObject: ICategoriesData = {
					category: mainCategories[i].Category,
					parentCategory: mainCategories[i].Parent,
				};
				self._categoriesRecieved.push(categoryObject);

				i++;
			}

			// initialise our dropdown
			self.createDropDownCategory(categoriesList);
		}

		/**
		* Initialise our categories which are dependant on our parent category
		*/
		private initialiseSubCategories(dataMain: any): void {
			var self = this;

			var mainSubCategories: any = dataMain;

			var subCategoriesList: string[] = [];
			var i: number = 0;
			while (mainSubCategories[i]) {

				var subCategoryObject: ICategoriesData = {
					category: mainSubCategories[i].Category,
					parentCategory: mainSubCategories[i].Parent,
				};
				self._subCategoriesRecieved.push(subCategoryObject);

				i++;
			}
		}
	}

	/**
	* Register event listener for window size
	*/
	document.addEventListener("DOMContentLoaded", function () {
		//var main = new Main();
		console.log("DomContentLoaded");
	});

	function start()
	{
		var main = new Main();
		console.log("Initializing main");
	}
	window.onload = start;

	/**
	* Capture on window resize event
	* Adjust the css classes accordingly
	*/
	//$(new Function("var game = new Main();"));

	function onWindowResize() {
		var w = window.outerWidth;
		var h = window.outerHeight;
		var txt = "Window size: width=" + w + ", height=" + h;
		//console.log(txt);
	}
}