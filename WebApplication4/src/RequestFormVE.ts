﻿/************************************************************************************
* Description: This class is used for filling out the job informations and sending it to server
* File: RequestFormVE.ts
* Author: branco
************************************************************************************/

///<reference path='AllReferences.ts' />

module Request {
	export class InputFields {
		/** Field name*/
		fieldName: string;

		/** City text box */
		inputBox: any;
	
		/** Warning paragraph for text box*/
		warningParagraph: HTMLParagraphElement;

		/** Second warning paragraph for format */
		warningParagraphFormat: HTMLParagraphElement;

	}

	export class RequestFormVE {
		/** Constant used as url parameter for categories*/
		private categoryTag: string = "Cat";

		/** Constant used as url parameter for categories*/
		private optionsTag: string = "SubCat";

		/** City text box */
		private cityTextBox: any;

		/** Job description text box */
		private jobDescriptionTextBox: any;

		/** Name text box */
		private nameTextBox: any;

		/** Surname text box */
		private surnameTextBox: any;

		/** Phone number text box */
		private phoneNumberTextBox: any;

		/** Email address text box*/
		private emailAddressTextBox: any;

		/** Send button*/
		private sendRequestButton: any;

		/** Back button*/
		private backButton: any;

		/** Category list transfered from homepage*/
		private category: string;

		/** Options list transfered from homepage*/
		private options: string;

		/** Warning paragraphs */
		private warningStringCity: HTMLParagraphElement;

		/** Warning paragraphs */
		private warningStringJobDescription: HTMLParagraphElement;

		/** Warning paragraphs */
		private warningStringName: HTMLParagraphElement;

		/** Warning paragraphs */
		private warningStringSurname: HTMLParagraphElement;

		/** Warning paragraphs */
		private warningStringPhoneNumber: HTMLParagraphElement;

		/** Warning paragraphs */
		private warningStringPhoneNumberFormat: HTMLParagraphElement;

		/** Warning paragraphs */
		private warningStringEmailAddress: HTMLParagraphElement;

		/** Warning paragraphs */
		private warningStringEmailAddressFormat: HTMLParagraphElement;

		/** Array of all warning paragraphs */
		private _inputFields: InputFields[];

		constructor() {
			var self = this;

			self._inputFields = [];
			self.initialize();
			self.addEventListeners();
			self.getValuesFromURL();
		}

		/**
		* Initialise with all elements from the dome
		*/
		private initialize(): void {
			var self = this;
			var inputField: InputFields = new InputFields;

			inputField.fieldName = "city";
			inputField.inputBox = document.getElementById("cityTextBox");
			inputField.warningParagraph = <HTMLParagraphElement> document.getElementById("warningStringCity");
			self._inputFields.push(inputField);

			inputField = new InputFields;
			inputField.fieldName = "jobDescription";
			inputField.inputBox = document.getElementById("jobDescriptionTextBox");
			inputField.warningParagraph = <HTMLParagraphElement> document.getElementById("warningStringJobDescription");
			self._inputFields.push(inputField);

			inputField = new InputFields;
			inputField.fieldName = "name";
			inputField.inputBox = document.getElementById("nameTextBox");
			inputField.warningParagraph = <HTMLParagraphElement> document.getElementById("warningStringName");
			self._inputFields.push(inputField);

			inputField = new InputFields;
			inputField.fieldName = "surname";
			inputField.inputBox = document.getElementById("surnameTextBox");
			inputField.warningParagraph = <HTMLParagraphElement> document.getElementById("warningStringSurname");
			self._inputFields.push(inputField);

			inputField = new InputFields;
			inputField.fieldName = "phone";
			inputField.inputBox = document.getElementById("phoneNumberTextBox");
			inputField.warningParagraph = <HTMLParagraphElement> document.getElementById("warningStringPhoneNumber");
			inputField.warningParagraphFormat = <HTMLParagraphElement> document.getElementById("warningStringPhoneNumberFormat");
			self._inputFields.push(inputField);

			inputField = new InputFields;
			inputField.fieldName = "email";
			inputField.inputBox = document.getElementById("emailAddressTextBox");
			inputField.warningParagraph = <HTMLParagraphElement> document.getElementById("warningStringEmailAddress");
			inputField.warningParagraphFormat = <HTMLParagraphElement> document.getElementById("warningStringEmailAddressFormat");
			self._inputFields.push(inputField);

			self.sendRequestButton = document.getElementById("sendRequestButton");

			self.backButton = document.getElementById("backButton");

		}

		/**
		* Set event listeners to buttons;
		*/
		private addEventListeners(): void {
			var self = this;

			self.sendRequestButton.addEventListener('click', function () {
				return self.insertNewJob();
			});

			self.backButton.addEventListener('click', function () {
				return self.goBack();
			});
		}

		/**
		* Back button pressed
		*/
		private goBack() {
			var self = this;

			window.history.back();
		}

		/**
		* Read sent values from URL
		*/
		private getValuesFromURL() {
			var self = this;

			self.category = self.getUrlParameter(self.categoryTag);
			self.options = self.getUrlParameter(self.optionsTag);

			console.log(self.category);
			console.log(self.options);
		}

		/**
		* Get parameters
		*/
		private getUrlParameter(sParam: string): string {
			var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				sURLVariables = sPageURL.split('&'),
				sParameterName: any,
				i: any;

			for (i = 0; i < sURLVariables.length; i++) {
				sParameterName = sURLVariables[i].split('=');

				if (sParameterName[0] === sParam) {
					return sParameterName[1] === undefined ? true : sParameterName[1];
				}
			}
		}

		/**
		* Check if user has entered all obligatory fields
		*/
		private verifySelections(): boolean {

			var self = this;
			var error: boolean = false;
			for (var i = 0; i < self._inputFields.length; i++) {
				if (self._inputFields[i].inputBox.value == "") {
					// Show warning
					error = true;
					self._inputFields[i].warningParagraph.style.visibility = 'visible';
				}
				else {
					self._inputFields[i].warningParagraph.style.visibility = 'hidden';
				}
			}
			if (error) {
				return false;
			}

			// Final check: formating of number and e-mail
			for (var i = 0; i < self._inputFields.length; i++) {
				if (self._inputFields[i].warningParagraphFormat != undefined) {
					// TODO: add phone and e-mail verification
				}
			}

			return true;
		}

		/**
		* Write the data to our server
		*/
		private insertNewJob(): void {
			var self = this;
			if (self.verifySelections()) {
				$.ajax({
					type: "POST",
					url: 'http://localhost:8080/insertJob.php',
					contentType: "application/json; charset=utf-8",
					data: {
						city: self._inputFields[0].inputBox.value,
						comment: self._inputFields[1].inputBox.value,
						name: self._inputFields[2].inputBox.value,
						surname: self._inputFields[3].inputBox.value,
						phone: self._inputFields[4].inputBox.value,
						email: self._inputFields[5].inputBox.value,
					},
					dataType: "json",
					//success: AjaxSucceeded,
					//error: AjaxFailed
				});
			}
		}

	}

}