﻿using System.Web;
using System.Web.Optimization;

namespace WebApplication4
{
	public class BundleConfig
	{
		// For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
						"~/Scripts/jquery-{version}.js"));

			// Use the development version of Modernizr to develop with and learn from. Then, when you're
			// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
						"~/Scripts/modernizr-*"));

			// Importing javascript files
			bundles.Add(new ScriptBundle("~/bundles/jsfiles").Include(
				"~/src/ServerResponse/Response1.js",
				"~/src/app.js",
				"~/src/DropDownMenuVE.js",
				"~/src/JobListVE.js",
				"~/src/JobTileVE.js",
				"~/src/RequestFormVE.js"
				));

			bundles.Add(new StyleBundle("~/Content/css").Include(
					  "~/Content/bootstrap.css",
					  "~/Content/site.css"));

			bundles.Add(new StyleBundle("~/site/RadisaTypescriptFinall/css").Include(
					  "~/Less/homepage.css",
					  "~/Content/bootstrap/css/bootstrap.min.css",
					  "~/Content/bootstrap/css/full-width-pics.css",
					  "~/Content/bootstrap/css/post-job.css",
					  "~/Content/bootstrap/css/step-by-step.css",
					  "~/Content/bootstrap/css/style"
				));
		}
	}
}
