﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication4
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				name: "Homepage",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action="Homepage" }
				);

			// This is the default homepage if we enter just our url
			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Homepage", id = UrlParameter.Optional }
			);
		}
	}
}
