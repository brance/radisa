﻿using WebApplication4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication4.Services
{
	public class ValueRepository
	{
		public Value[] GetAllValues()
		{
			return new Value[]
		{
			 new Value
			 {
				  id = 1,
				  name = "Glenn Block"
			 },
			 new Value
			 {
				  id = 2,
				  name = "Dan Roth"
			 }
		};
		}
	}
}