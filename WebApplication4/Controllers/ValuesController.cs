﻿using WebApplication4.Models;
using WebApplication4.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApplication4.Controllers
{
	[EnableCors(origins: "http://localhost", headers: "*", methods: "*")]
	public class ValuesController : ApiController
	{

		private ValueRepository valueRepository;

		public ValuesController()
		{
			this.valueRepository = new ValueRepository();
		} 

		// GET api/values
		public Value[] Get()
		{
			return this.valueRepository.GetAllValues();
		}

		// GET api/values/5
		public string Get(int id)
		{
			return "value";
		}

		// POST api/values
		public void Post([FromBody]string value)
		{
		}

		// PUT api/values/5
		public void Put(int id, [FromBody]string value)
		{
		}

		// DELETE api/values/5
		public void Delete(int id)
		{
		}
	}
}
