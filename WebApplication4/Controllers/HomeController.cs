﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http.Cors;

namespace WebApplication4.Controllers
{
	[EnableCors(origins: "http://localhost", headers: "*", methods: "*")]
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewBag.Title = "Home Page";

			return View();
		}

		public ActionResult Homepage()
		{
			ViewBag.Title = " My Home Page";
			return View();
		}
	}
}
